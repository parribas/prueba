#Ejercicios básicos de programación
#Crear una clase Rectángulo, con atributos base y altura. Crear también el constructor de la clase
#y los métodos necesarios para calcular el área y el perímetro. 

class Rectangulo:
    """nos pide que tenga 2 metodos, el area y el perimetro"""
    def __init__(self, b, h):
        self.base = b
        self.altura = h

    def area(self):
        a = self.base * self.altura
        return a

    def perimetro(self):
        p = (2 * self.base) + (2 * self.altura)
        return p

rectángulo01 = Rectangulo(2, 4)
rectángulo02 = Rectangulo(3, 6)
rectángulo03 = Rectangulo(10, 20)

print("El area del rectángulo es", rectángulo03.area()," y su perímetro", rectángulo03.perimetro())
