class Empleado:
    #creo la clase empleado y que tiene tres atributos, el nombre, el salario y la tasa; y ademas incluyo el calculo
    def __init__(self, nombre, salario, tasa, antigüedad):
        #el init es la funcion constructor, inicializa cada uno de los atributos que le hayamos puesto cada vez que la llamamos
        self.__nombre = nombre
        self.__salario = salario
        self.__tasa = tasa
        self.__antigüedad = antigüedad
        #el self asigna el atributo al empleado

    def CalculoImpuestos(self):
        self.__impuestos = self.__salario*self.__tasa
        print ("El empleado", self.__nombre, "debe pagar", self.__impuestos)
        return self.__impuestos

    def DescuentoAntigüedad(self):
        if self.__antigüedad > 1:
            self.__tasa = self.__tasa - 0.1
        else: 
            self.__tasa = self.__tasa
        return self.__tasa

def displayCost(total):
    print("Los impuestos a pagar en total son", total, "euros")

emp1 = Empleado("Pepe", 20000, 0.35, 10)
emp2 = Empleado("Ana", 30000, 0.30, 0.5)
empleados = [emp1, emp2, Empleado("Luis", 10000, 0.10, 0), Empleado("Luisa", 25000, 0.15, 2)]
total = 0
for emp in empleados:
    total += emp.CalculoImpuestos()

displayCost(total)