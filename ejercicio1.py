#la primera palabra debe ser en mayúscula
class Cuadrado:
    """nos pide que tenga 2 metodos, el area y el perimetro"""
    def __init__(self,l=1):
        self.lado = l 
        #si pongo (self):
        #self.lado = 0  . le estoy dando un valor fijo

    def area(self):
        a = self.lado **2
        return a

    def perimetro(self):
        p = 4 * self.lado
        return p

cuadrado01 = Cuadrado(2)
#si no pongo nada en el constructor, me toma el valor que le he dado yo por defecto, 
#sino elige el que yo le doy
cuadrado02 = Cuadrado()

print("El area del cuadrado es", cuadrado01.area()," y su perímetro", cuadrado01.perimetro())
