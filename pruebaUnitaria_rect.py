import hoja_1
import unittest

class RectanguloTestCase(unittest.TestCase):
    def testRectanguloPerimetro(self):
        rectangulo01 = hoja_1.Rectangulo(1,2)
        perimetro1 = rectangulo01.perimetro()
        self.assertEqual(perimetro1,6)
    
    def testRectanguloArea(self):
        rectangulo01 = hoja_1.Rectangulo(1,2)
        area1 = rectangulo01.area()
        self.assertEqual(area1,2)

if __name__ == '__main__':
    unittest.main()